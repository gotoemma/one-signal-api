<?php

namespace Gotoemma\OneSignalApiBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('one_signal_api');

        $rootNode
            ->children()
                ->scalarNode('app_id')->end()
                ->scalarNode('app_auth_key')->end()
                ->scalarNode('user_auth_key')->end()
            ->end();

        return $treeBuilder;
    }
}
