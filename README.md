# OneSignal PHP API Symfony Integration

Symfony Integration for [OneSignal PHP API](https://github.com/norkunas/onesignal-php-api).

### Dependencies

- Symfony 3.0
- Guzzle 6
- Norkunas OneSignal PHP API

### Install

```console
$ composer require gotoemma/one-signal-api
```

### Your projects config.yml

```yaml
one_signal_api:
    app_id:         %one_signal_api_app_id%
    app_auth_key:   %one_signal_api_app_auth_key%
    user_auth_key:  %one_signal_api_user_auth_key%
```

### How to use the service

```php
$service = $this->get('one_signal_api');
```